import React from "react";
import CardItem from "../CardItem/CardItem";
import styles from "./Card.module.scss";

const Card = () => {
  const mockdatas = [
    { id: 1, src: "/assets/images/img-1.jpg", text: "TEST1", label: "TEST1" },
    { id: 2, src: "/assets/images/img-2.jpg", text: "TEST1", label: "TEST2" },
    { id:3, src: "/assets/images/img-3.jpg", text: "TEST1", label: "TEST3" },
    { id:4, src: "/assets/images/img-4.jpg", text: "TEST1", label: "TEST4" },
    { id:5, src: "/assets/images/img-5.jpg", text: "TEST1", label: "TEST5" },
    { id:6, src: "/assets/images/img-6.jpg", text: "TEST1", label: "TEST6" },
    { id:7, src: "/assets/images/img-7.jpg", text: "TEST1", label: "TEST7" },
    { id:8, src: "/assets/images/img-8.jpg", text: "TEST1", label: "TEST8" },
    { id:9, src: "/assets/images/img-9.jpg", text: "TEST1", label: "TEST9" },
  ];

  return (
    <div className={styles.cards}>
      <h1  className={styles.card_title}>Cards</h1>
      <div className={styles.cards_container}>
        <div className={styles.cards_wrapper}>
          <ul className={styles.cards_items}>
            {mockdatas
              ? mockdatas.map((res, i) => (
                  <CardItem
                    src={res.src}
                    text={res.text}
                    label={res.label}
                    key={res.id}
                  />
                ))
              : ""}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Card;
