import React, { useState } from "react";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHeadphones,
  faTimes,
  faBars,
} from "@fortawesome/free-solid-svg-icons";
import LanguageSwitcher from "../LanguageSwitcher/LanguageSwitcher";
import { withTranslation } from "next-i18next";
import styles from "./Header.module.scss";

const Header = ({ t }) => {
  const [click, setClick] = useState(false);
  const closeMobileMenu = () => setClick(false);
  const handleClick = () => {
    setClick(!click);
  };

  return (
    <div className={styles.navbar}>
      <div className={styles.navbar_container}>
        <Link href="/">
          <div className={styles.navbar_logo}>
            <FontAwesomeIcon icon={faHeadphones} />
            <label className={styles.title}>TEST</label>
          </div>
        </Link>
      </div>
      <div className={styles.menu_icon} onClick={handleClick}>
        <FontAwesomeIcon icon={click ? faTimes : faBars} />
      </div>
      <ul className={click ? styles.nav_menu_active : styles.nav_menu}>
        <li className={styles.nav_item} key={"home"}>
          <Link href="/" onClick={closeMobileMenu}>
            <div className={styles.nav_links}>{t("home")}</div>
          </Link>
        </li>
        <li className={styles.nav_item} key={"aboutus"}>
          <Link href="/aboutus" onClick={closeMobileMenu}>
            <div className={styles.nav_links}>{t("aboutus")}</div>
          </Link>
        </li>
        <li className={styles.nav_item} key={"promotions"}>
          <Link href="/404" onClick={closeMobileMenu}>
            <div className={styles.nav_links}>{t("promotions")}</div>
          </Link>
        </li>
        <li className={styles.nav_item} key={"service"}>
          <Link href="/404" onClick={closeMobileMenu}>
            <div className={styles.nav_links}>{t("service")}</div>
          </Link>
        </li>
        <li className={styles.nav_item} key={"contactus"}>
          <Link href="/404" onClick={closeMobileMenu}>
            <div className={styles.nav_links}>{t("contactus")}</div>
          </Link>
        </li>
        <li className={styles.nav_item} key={"languageswitche"}>
          <div className={styles.nav_language_switcher}>
            <LanguageSwitcher />
          </div>
        </li>
      </ul>
    </div>
  );
};

export default withTranslation()(Header);
