import React from "react";
import Link from "next/link";
import styles from "./CardItem.module.scss";

const CardItem = ({ src, text, label }) => {
  return (
    <>
      <li className={styles.cards_item}>
        <Link href={"/"}>
          <div className={styles.cards_item_link}>
            <figure
              className={styles.cards_item_pic_wrap}
              data-category={label}
            >
              <img
                className={styles.cards_item_img}
                alt="TravelImage"
                src={src}
              />
            </figure>
            <div className={styles.cards_item_info}>
              <h5 className={styles.cards_item_text}>{text}</h5>
            </div>
          </div>
        </Link>
      </li>
    </>
  );
};

export default CardItem;
