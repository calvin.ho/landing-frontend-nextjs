import Link from "next/link";
import { i18n } from "next-i18next";
import { useContext } from "react";
import { I18nContext } from "next-i18next";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
}));

const LanguageSwitcher = () => {
  const classes = useStyles();
  const {
    i18n: { language },
  } = useContext(I18nContext);
  console.log(language);
  return (
    <>
      <div className={classes.root}>
        <ButtonGroup
          variant="text"
          color="primary"
          aria-label="text primary button group"
        >
          <Button>
            <Link href="/" locale={"tc"}>
              TC
            </Link>
          </Button>

          <Button>
            <Link href="/" locale={"en"}>
              EN
            </Link>
          </Button>
        </ButtonGroup>
      </div>
    </>
  );
};

export default LanguageSwitcher;
