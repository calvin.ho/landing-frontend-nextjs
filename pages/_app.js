import '../styles/globals.scss';
import React, {useEffect} from "react";
import Head from "next/head";
// import PropTypes from "prop-types";
import ContextProvider from "../context/ContextProvider";
import Layout from "../components/Layout";
import CssBaseline from "@material-ui/core/CssBaseline";
import FullPageLoader from "../utils/FullPageLoader";
import { appWithTranslation } from 'next-i18next';

const MyApp = ({ Component, pageProps, userAgent }) => {
  const isMobile = Boolean(
    userAgent.match(
      /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i
    )
  );

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);


  useEffect(() => {
    if ("serviceWorker" in navigator) {
      window.addEventListener("load", function () {
        navigator.serviceWorker.register("/sw.js").then(
          function (registration) {
            console.log(
              "Service Worker registration successful with scope: ",
              registration.scope
            );
          },
          function (err) {
            console.log("Service Worker registration failed: ", err);
          }
        );
      });
    }
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>NextJS</title>
      </Head>
      <Layout>
        <CssBaseline />
        <ContextProvider>
          <FullPageLoader />
          <Component
            {...pageProps}
            userAgent={userAgent}
            deviceType={isMobile ? "mobile" : "desktop"}
          />
        </ContextProvider>
      </Layout>
    </React.Fragment>
  );
};

MyApp.getInitialProps = async ({ Component, ctx }) => {
  let pageProps = {};

  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx);
  }

  const userAgent =
    typeof window === "undefined"
      ? ctx.req.headers["user-agent"]
      : window.navigator.userAgent;

  return {
    pageProps,
    userAgent: userAgent,
    namespacesRequired: ["common"],
  };
};

export default appWithTranslation(MyApp);
