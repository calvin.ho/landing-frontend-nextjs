import { useEffect } from 'react';
import Router from 'next/router';

const Page404 = () => {
    useEffect(() => {
        Router.push('/');
    });

    return <></>;
};

export default Page404;
