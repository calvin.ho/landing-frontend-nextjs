import React  from "react";
import Header from "../components/Header/Header";
// import HeroSection from "../components/HeroSection/HeroSection";
import Card from "../components/Card/Card";
import Footer from "../components/Footer/Footer";
import { withTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

const Home = () => {
  return (
    <>
      <Header />
      {/* <HeroSection /> */}
      <Card />
      <Footer />
    </>
  );
}

export async function getServerSideProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
    },
  };
}

export default withTranslation('common')(Home);
