import React, { useContext } from "react";
import { Context } from "../context/ContextProvider";

const FullPageLoader = () => {
  const { loader } = useContext(Context);

  return (
    <div
      style={
        loader
          ? {
              backgroundColor: "rgba(161,25,79,0.8)",
              position: "fixed",
              width: "100%",
              height: "100%",
              zIndex: 1000,
              top: "0px",
              left: "0px",
            }
          : null
      }
    ></div>
  );
};

export default FullPageLoader;
