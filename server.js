const next = require("next");
const port = parseInt(process.env.PORT, 10) || 3001;
const dev = process.env.NODE_ENV !== "production" && process.env.NODE_ENV !== "staging";
const app = next({ 
    dev,
    dir: '.', 
    quiet: false 
});
const handle = app.getRequestHandler();

const { createServer } = require("http");
const { join } = require("path");
const { parse } = require("url");


app.prepare().then(() => {
  createServer((req, res) => {
    const parsedUrl = parse(req.url, true);
    const { pathname } = parsedUrl;
    if (pathname === "/sw.js" || pathname.startsWith("/workbox-")) {
      const filePath = join(__dirname, ".next", pathname);
      app.serveStatic(req, res, filePath);
    } else {
      handle(req, res, parsedUrl);
    }
  }).listen(port, () => {
    console.log(`> Ready on http://localhost:${port}`);
  });
});