FROM node:15.2.0-buster-slim

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install
RUN npm install pm2 -g
COPY . .

RUN npm run build
EXPOSE 8888

CMD ["pm2-runtime", "start", "ecosystem.config.js", "--env", "production"]
