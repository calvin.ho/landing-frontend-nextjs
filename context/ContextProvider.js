import React, { createContext, useState } from "react";

export const Context = createContext(null);

const ContextProvider = (props) => {

  const [errorMessage, setErrorMessage] = useState(null);
  const storeErrorMessage = (data) => {
    setErrorMessage(data);
  };

  const [loader, setLoader] = useState(false);
  const storeLoader = (data) => {
    setLoader(data);
  };

  return (
    <Context.Provider value={{ errorMessage, storeErrorMessage, loader, storeLoader }}>
      {props.children}
    </Context.Provider>
  );
};

export default ContextProvider;