const path = require("path");
require("dotenv").config();
const withPWA = require("next-pwa");
const withSass = require("@zeit/next-sass");
const { i18n } = require('./next-i18next.config')

module.exports = withPWA(
  withSass({
    cssModules: true,
    pwa: {
      dest: "public",
      scope: "/",
    },
    env: {},
    publicRuntimeConfig: {},
    webpack: (config) => {
      config.resolve.alias["components"] = path.join(__dirname, "components");
      config.resolve.alias["public"] = path.join(__dirname, "public");
      return config;
    }
  })
);

module.exports = {
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
  i18n,
};
